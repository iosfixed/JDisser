import re
import time

import numpy as np
import tqdm as tqdm
from numpy import sqrt, pi, dot, sin, cos, arctan2, cross, arcsin, arctan, math, random

from utils import reduce, ephem
from consts import *
from libcelestial import ephem

import scipy.stats as stats
import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

np.random.seed(666)
# Все параметры записывают перед процедурками
interval = 1 # интервал
n2 = 12 # кол наблюдений
kk = 5.2 # квантиль фишера
kt = 1000 # кол точек
kd = 1500 # дни

# t0 = 2455647.46557825
# a = 0.77687777440592381
# e = 0.68510387692466500
# i = 2.77916532987703325
# W = 3.2670520191630
# w = 0.326641461628737
# M0 = 4.80579307699276
#
# a2 = 1.00006565717335842
# e2 = 0.01708278413879609
# i2 = 0.40910070729284629
# W2 = 0.00001440473697043
# w2 = -4.483954375681416
# M02 = 1.409510836394793

#объект
t0 = 2453993.38341912
a = 2.6326375423103763
e = 0.68296434449828680
i = 19.0332 * rad
W = 7.05843 * rad
w = 53.0262 * rad
M0 = 348.065 * rad

#земля
a2 = 1.00000267492667613
e2 = 0.01635277395291997
i2 = 0.40907645295092280
W2 = 0.00001077572125351
w2 = 1.805121824028950
M02 = -1.917372742782691


class Sat:
    def __init__(self, init=None, ptype='kep', t0=t0, mu=mu_geo):
        self.t0 = t0
        self.mu = mu

        if (ptype == 'kep'):
            if (init == None):
                init = [42164, 0, 0, 0, 0, 0]  # дефолт
            self.a, self.e, self.i, self.W, self.w, self.M0 = init
            self.cartesian()

        if (ptype == 'qv'):
            self.x, self.y, self.z, self.vx, self.vy, self.vz = init
            self.findOrbit()

    ########################## Вычисления ##########################

    ##### Функция определения декартовых координат #####
    def cartesian(self, t=t0, dt=None):
        a, e, i, W, w, M0 = self.get('kep')
        mu = self.mu
        t0 = self.t0

        A = [[cos(W), -sin(W), 0],
             [sin(W), cos(W), 0],
             [0, 0, 1]]
        A = np.matrix(A)

        B = [[1, 0, 0],
             [0, cos(i), -sin(i)],
             [0, sin(i), cos(i)]]
        B = np.matrix(B)

        C = [[cos(w), -sin(w), 0],
             [sin(w), cos(w), 0],
             [0, 0, 1]]
        C = np.matrix(C)

        R = A * B * C  # поворотная матрица

        n = sqrt(mu * a ** (-3))

        if (dt):
            M = M0 + n * dt
        else:
            M = M0 + n * (t - t0)

        M = reduce(M)


        # Численное решение уравнения Кеплера
        E = M
        while (abs(E - e * sin(E) - M) > 1e-12):
            E = E - (E - e * sin(E) - M) / (1 - e * cos(E))

        q = np.matrix([a * (cos(E) - e), a * sqrt(1 - e ** 2) * sin(E), 0])
        dq = np.matrix([-a * n * sin(E) / (1 - e * cos(E)), a * sqrt(1 - e ** 2) * cos(E) * n / (1 - e * cos(E)), 0])

        #        reduced = reduce(M) - M0

        q = R * q.T
        dq = R * dq.T

        self.x, self.y, self.z = q.A1
        self.vx, self.vy, self.vz = dq.A1
        return [self.x, self.y, self.z, self.vx, self.vy, self.vz]

    ##### Функция для вичисления частных производных по кеплеровым элементам #####
    def derivatives(self, t=t0, dt=None):
        a, e, i, W, w, M0 = self.get('kep')
        mu = self.mu
        t0 = self.t0

        A = [[cos(W), -sin(W), 0],
             [sin(W), cos(W), 0],
             [0, 0, 1]]
        A = np.matrix(A)

        B = [[1, 0, 0],
             [0, cos(i), -sin(i)],
             [0, sin(i), cos(i)]]
        B = np.matrix(B)

        C = [[cos(w), -sin(w), 0],
             [sin(w), cos(w), 0],
             [0, 0, 1]]
        C = np.matrix(C)

        R = A * B * C

        n = sqrt(mu * a ** (-3))

        if (dt):
            M = M0 + n * dt
        else:
            M = M0 + n * (t - t0)

        M = reduce(M)

        # Численное решение уравнения Кеплера
        E = M
        while (abs(E - e * sin(E) - M) > 1e-12):
            E = E - (E - e * sin(E) - M) / (1 - e * cos(E))

        q = np.matrix([a * (cos(E) - e), a * sqrt(1 - e ** 2) * sin(E), 0])

        reduced = n * (t - t0)  # M - M0

        # частные производные
        dksi_da = cos(E) - e + 3 / 2 * (reduced * sin(E)) / (1 - e * cos(E))
        deta_da = sqrt(1 - e ** 2) * (sin(E) - 3 / 2 * (reduced * cos(E)) / (1 - e * cos(E)))
        da = np.matrix([dksi_da, deta_da, 0])
        dxyz_da = R * da.T

        dksi_de = -a * (1 + (sin(E) ** 2) / (1 - e * cos(E)))
        deta_de = a * sqrt(1 - e ** 2) * sin(E) * ((cos(E) / (1 - e * cos(E))) - (e / (1 - e ** 2)))
        de = np.matrix([dksi_de, deta_de, 0])
        dxyz_de = R * de.T

        dksi_dM0 = - (a * sin(E)) / (1 - e * cos(E))
        deta_dM0 = (a * (sqrt(1 - e ** 2)) * cos(E)) / (1 - e * cos(E))
        dM0 = np.matrix([dksi_dM0, deta_dM0, 0])
        dxyz_M0 = R * dM0.T

        dB = [[0, 0, 0],
              [0, -sin(i), -cos(i)],
              [0, cos(i), -sin(i)]]
        dB = np.matrix(dB)

        dA = [[-sin(W), -cos(W), 0],
              [cos(W), -sin(W), 0],
              [0, 0, 0]]
        dA = np.matrix(dA)

        dC = [[-sin(w), -cos(w), 0],
              [cos(w), -sin(w), 0],
              [0, 0, 0]]
        dC = np.matrix(dC)

        dxyz_di = A * dB * C * q.T
        dxyz_dw = A * B * dC * q.T
        dxyz_dW = dA * B * C * q.T
        dxyz_da_dM0 = np.concatenate([dxyz_da, dxyz_de, dxyz_di, dxyz_dw, dxyz_dW, dxyz_M0], axis=1)
        return dxyz_da_dM0

    ##### Функция получения кеплеровых элементов #####
    def findOrbit(self):
        q = [self.x, self.y, self.z]
        v = [self.vx, self.vy, self.vz]

        r = sqrt(dot(q, q))
        V = sqrt(dot(v, v))
        s = dot(q, v)
        h = cross(q, v)

        k = sqrt(self.mu)

        # Большая полуось
        a = 1 / abs(2. / r - V ** 2 / k ** 2)

        # Эксцентрисистет
        e = sqrt(s * s / (k * k * a) + (1 - r / a) ** 2)

        # Средняя аномалия:
        dy = s / (e * k * sqrt(a))
        dx = (a - r) / (a * e)
        E0 = arctan2(dy, dx)
        M0 = E0 - e * sin(E0)

        # Долгота восходящего узла:
        W = reduce(arctan2(h[0], -h[1]))

        # Наклонение
        i = arctan2(sqrt(h[0] ** 2 + h[1] ** 2), h[2])

        # Аргумент перицентра:
        p = a * (1 - e ** 2)

        dy = sqrt(p) * s
        dx = k * (p - r)
        vv = arctan2(dy, dx)

        if (sin(i) != 0):
            dy = self.z / sin(i)
            dx = self.x * cos(W) + self.y * sin(W)
            uu = arctan2(dy, dx)
        else:
            uu = 0

        w = uu - vv
        while (w < 0):
            w += 2 * pi

        self.a, self.e, self.i, self.W, self.w, self.M0 = a, e, i, W, w, M0
        return [self.a, self.e, self.i, self.W, self.w, self.M0]

    ##### Функция определения частных производных по alpha, delta #####
    def derivatives2(self, t=t0, dt=None):
        xr = aster.x - earth.x
        yr = aster.y - earth.y
        zr = aster.z - earth.z

        r = sqrt(xr ** 2 + yr ** 2 + zr ** 2)

        alpha = reduce(np.arctan2(yr, xr))
        delta = arcsin(zr / r)

        dalpha_dx = -sin(alpha) / (r * cos(delta))
        ddelta_dx = -cos(alpha) * sin(delta) / r

        dalpha_dy = cos(alpha) / (r * cos(delta))
        ddelta_dy = -sin(alpha) * sin(delta) / r

        dalpha_dz = 0
        ddelta_dz = cos(delta) / r

        dalpha_dq = np.matrix([dalpha_dx, dalpha_dy, dalpha_dz])
        ddelta_dq = np.matrix([ddelta_dx, ddelta_dy, ddelta_dz])
        dalpha_dq_ddelta_dq = np.concatenate([dalpha_dq, ddelta_dq])
        return dalpha_dq_ddelta_dq

    ##### Функция определения частных производных по alpha, delta со скоростями #####
    def derivatives3(self, t=t0, dt=None):
        a, e, i, W, w, M0 = self.get('kep')
        mu = self.mu
        t0 = self.t0

        A = [[cos(W), -sin(W), 0],
             [sin(W), cos(W), 0],
             [0, 0, 1]]
        A = np.matrix(A)

        B = [[1, 0, 0],
             [0, cos(i), -sin(i)],
             [0, sin(i), cos(i)]]
        B = np.matrix(B)

        C = [[cos(w), -sin(w), 0],
             [sin(w), cos(w), 0],
             [0, 0, 1]]
        C = np.matrix(C)

        R = A * B * C

        n = sqrt(mu * a ** (-3))

        if (dt):
            M = M0 + n * dt
        else:
            M = M0 + n * (t - t0)
        M = reduce(M)

        # Численное решение уравнения Кеплера
        E = M
        while (abs(E - e * sin(E) - M) > 1e-12):
            E = E - (E - e * sin(E) - M) / (1 - e * cos(E))

        q = np.matrix([a * (cos(E) - e), a * sqrt(1 - e ** 2) * sin(E), 0])
        dq = np.matrix([-a * n * sin(E) / (1 - e * cos(E)), a * sqrt(1 - e ** 2) * cos(E) * n / (1 - e * cos(E)), 0])
        reduced = n * (t - t0)  # M - M0

        # частные производные
        dksi_da = cos(E) - e + 3 / 2 * (reduced * sin(E)) / (1 - e * cos(E))
        ddksi_da = 1 / 2 * n * (sin(E) / (1 - e * cos(E))) + 3 / 2 * n * reduced * (
                    (cos(E) - e) / (1 - e * cos(E)) ** 3)
        deta_da = sqrt(1 - e ** 2) * (sin(E) - 3 / 2 * (reduced * cos(E)) / (1 - e * cos(E)))
        ddeta_da = - (n * sqrt(1 - e * e) * cos(E)) / (2 * (1 - e * cos(E))) + 3 / 2 * n * sqrt(
            1 - e ** 2) * reduced * (sin(E) / (1 - e * cos(E)) ** 3)
        da = np.matrix([dksi_da, deta_da, 0])
        dda = np.matrix([ddksi_da, ddeta_da, 0])
        dxyz_da = R * da.T
        dxyz_dda = R * dda.T

        dksi_de = -a * (1 + (sin(E) ** 2) / (1 - e * cos(E)))
        ddksi_de = - ((a * n * sin(E)) / (1 - e * cos(E)) ** 2) * (cos(E) + (cos(E) - e) / (1 - e * cos(E)))
        deta_de = a * sqrt(1 - e ** 2) * sin(E) * ((cos(E) / (1 - e * cos(E))) - (e / (1 - e ** 2)))
        ddeta_de = - (a * e * n * cos(E)) / (sqrt(1 - e ** 2) * (1 - e * cos(E))) - (
                    (a * n * sqrt(1 - e ** 2)) / (1 - e * cos(E)) ** 2) * (
                               sin(E) * sin(E) - cos(E) * (cos(E) - e) / (1 - e * cos(E)))
        de = np.matrix([dksi_de, deta_de, 0])
        dde = np.matrix([ddksi_de, ddeta_de, 0])
        dxyz_de = R * de.T
        dxyz_dde = R * dde.T

        dksi_dM0 = - (a * sin(E)) / (1 - e * cos(E))
        ddksi_dM0 = - (n * a * (cos(E) - e)) / (1 - e * cos(E)) ** 3
        deta_dM0 = (a * (sqrt(1 - e ** 2)) * cos(E)) / (1 - e * cos(E))
        ddeta_dM0 = - (n * a * sqrt(1 - e ** 2) * sin(E)) / (1 - e * cos(E)) ** 3
        dM0 = np.matrix([dksi_dM0, deta_dM0, 0])
        dxyz_M0 = R * dM0.T
        ddM0 = np.matrix([ddksi_dM0, ddeta_dM0, 0])
        ddxyz_M0 = R * ddM0.T

        dB = [[0, 0, 0],
              [0, -sin(i), -cos(i)],
              [0, cos(i), -sin(i)]]
        dB = np.matrix(dB)

        dA = [[-sin(W), -cos(W), 0],
              [cos(W), -sin(W), 0],
              [0, 0, 0]]
        dA = np.matrix(dA)

        dC = [[-sin(w), -cos(w), 0],
              [cos(w), -sin(w), 0],
              [0, 0, 0]]
        dC = np.matrix(dC)

        dxyz_di = A * dB * C * q.T
        dxyz_dw = A * B * dC * q.T
        dxyz_dW = dA * B * C * q.T
        dxyz_ddi = A * dB * C * dq.T
        dxyz_ddw = A * B * dC * dq.T
        dxyz_ddW = dA * B * C * dq.T

        dxyz_da_dM0 = np.concatenate([dxyz_da, dxyz_de, dxyz_di, dxyz_dw, dxyz_dW, dxyz_M0], axis=1)
        dxyz_dda_ddM0 = np.concatenate([dxyz_dda, dxyz_dde, dxyz_ddi, dxyz_ddw, dxyz_ddW, ddxyz_M0], axis=1)
        dxyz_da_dM0_dda_ddM0 = np.concatenate([dxyz_da_dM0, dxyz_dda_ddM0])
        return dxyz_da_dM0_dda_ddM0

    ##### Процедуры установки новых параметров #####
    def set(self, settings, type='kep'):
        if (type == 'kep'):
            self.a, self.e, self.i, self.W, self.w, self.M0 = settings
            self.cartesian()

        if (type == 'qv'):
            self.x, self.y, self.z, self.vx, self.vy, self.vz = settings
            self.findOrbit()

    ##### Процедуры возвращающие параметры: ######
    def get(self, type='kep'):
        if (type == 'kep'):
            return [self.a, self.e, self.i, self.W, self.w, self.M0]


##### Создание объектов класса #####

aster = Sat([a, e, i, W, w, M0], ptype='kep', mu=mu_solar_au, t0=t0)
earth = Sat([a2, e2, i2, W2, w2, M02], ptype='kep', mu=mu_solar_au, t0=t0)

############## Метод наименьших квадратов ###############

##### Формирование моментов времени ######
tm = []
# for j in range(n2):
#     arr = (t0 + ((j+1) - math.trunc(n2/2)) * interval)
#     tm.append(arr)
# print('сутки:',tm[54]-tm[0])
#tt = []

tm = [2453974.77504250, 2453974.79553250, 2453974.80586250, 2453975.80620250, 2453975.82639251, 2453975.84370251,
      2453995.76306264, 2453995.77673264, 2453995.79040264, 2453995.80409264, 2453995.82592264, 2453995.82958264]

# for i in range(n2-1):
#     tm[i] = tm[i] + random.random()
# tm[0] = 2453974.77504250
# tm[11] = 2453995.82958264
# print(tm)


# tt = tm[11] - tm[0]
# tmn = np.zeros(22)
# for i in range(n2+2):
#     tmn[i] = tmn[i]
#     tm.append(tmn[i])
# tm[21] = 2453995.82958264
# print(tm)


# for j in range(n2-1):
#     arr = (2453974.77504250 + (21.054540140088648 / (n2-2)) * j)
#     tm.append(arr)
# tm.append(2453995.82958264)



##### Формируем ошибки наблюдений #####
ErrAlpha = []
#ErrAlpha = (np.loadtxt('modalph.txt'))*rad/3600
ErrAlpha = (np.random.normal(0, 1, n2)) * rad / 3600

ErrDelta = []
#ErrDelta = (np.loadtxt('moddelt.txt'))*rad/3600
ErrDelta = (np.random.normal(0, 1, n2)) * rad / 3600

##### Формируем альфа, дельта #####

alpha = []
delta = []
for j in range(n2):
    AstRaz = aster.cartesian(tm[j])
    ZemRaz = earth.cartesian(tm[j])
    RazX = aster.x - earth.x
    RazY = aster.y - earth.y
    RazZ = aster.z - earth.z

    RRaz = sqrt(RazX ** 2 + RazY ** 2 + RazZ ** 2)

    alph = reduce(np.arctan2(RazY, RazX))
    alpha.append(alph)
    delt = arcsin(RazZ / RRaz)
    delta.append(delt)

AlphaObs = ErrAlpha + alpha
DeltaObs = ErrDelta + delta

XZAI = aster.derivatives(t0)
XZDA = aster.derivatives2(t0)

##### Основной цикл #####
Decart = np.matrix(aster.cartesian(t0)).T

while True:

    matrixL = np.zeros(shape=(2 * n2, 1))
    matrixR = np.zeros(shape=(2 * n2, 6))
    RTL = np.zeros(6)
    RTR = np.zeros(shape=(6, 6))
    fq = 0

    for j in range(n2):
        XZAI = aster.derivatives(tm[j])
        AstRaz = aster.cartesian(tm[j])
        ZemRaz = earth.cartesian(tm[j])

        RazX = aster.x - earth.x
        RazY = aster.y - earth.y
        RazZ = aster.z - earth.z
        Decart = np.matrix(aster.cartesian(t0)).T

        RRaz = sqrt(RazX ** 2 + RazY ** 2 + RazZ ** 2)

        alph = reduce(np.arctan2(RazY, RazX))
        delt = arcsin(RazZ / RRaz)

        dalph_dx = -sin(alph) / (RRaz * cos(delt))
        ddelt_dx = -cos(alph) * sin(delt) / RRaz

        dalph_dy = cos(alph) / (RRaz * cos(delt))
        ddelt_dy = -sin(alph) * sin(delt) / RRaz

        dalph_dz = 0
        ddelt_dz = cos(delt) / RRaz

        # Нормальная матрица
        for k in range(6):
            for p in range(6):
                RTR[k][p] = RTR[k][p] + (dalph_dx * XZAI[0, k] + dalph_dy * XZAI[1, k]) * (
                            dalph_dx * XZAI[0, p] + dalph_dy * XZAI[1, p]) * (cos(DeltaObs[j])) * (cos(DeltaObs[j])) + (
                                        ddelt_dx * XZAI[0, k] + ddelt_dy * XZAI[1, k] + ddelt_dz * XZAI[2, k]) * (
                                        ddelt_dx * XZAI[0, p] + ddelt_dy * XZAI[1, p] + ddelt_dz * XZAI[2, p])

        for i in range(6):
            RTL[i] = RTL[i] + (dalph_dx * XZAI[0, i] + dalph_dy * XZAI[1, i]) * (alph - AlphaObs[j]) * (
                cos(DeltaObs[j])) * (cos(DeltaObs[j])) + (
                                 ddelt_dx * XZAI[0, i] + ddelt_dy * XZAI[1, i] + ddelt_dz * XZAI[2, i]) * (
                                 delt - DeltaObs[j])

        matrixL[2 * j - 1][0] = (alph - AlphaObs[j]) * cos(DeltaObs[j])
        matrixL[2 * j][0] = delt - DeltaObs[j]

        for i in range(6):
            matrixR[2 * j - 1][i] = (dalph_dx * XZAI[0, i] + dalph_dy * XZAI[1, i]) * cos(DeltaObs[j])
            matrixR[2 * j][i] = (ddelt_dx * XZAI[0, i] + ddelt_dy * XZAI[1, i] + ddelt_dz * XZAI[2, i])

        fq = fq + ((alph - AlphaObs[j]) * cos(DeltaObs[j])) ** 2 + (delt - DeltaObs[j]) ** 2

    ObrRTR = np.linalg.inv(RTR)

    # Тут что-то происходит непонятное с названиями переменных

    RORT = (aster.derivatives3(t0)) * ObrRTR * (aster.derivatives3(t0)).T  # P * ObrRTR * P.T
    RObrL = (matrixR * np.linalg.inv(aster.derivatives3(t0))).T * matrixL
    Mod = RORT * RObrL
    MDecart = Decart - Mod

    sigma = sqrt(fq / (2 * n2 - 6))
    matrixD = sigma ** 2 * RORT

    Decarton = Sat(MDecart.squeeze().tolist()[0], ptype='qv', mu=mu_solar_au, t0=t0)
    orbit = Decarton.get('kep')
    aster = Sat(orbit, ptype='kep', mu=mu_solar_au, t0=t0)
    # print(np.max(np.abs(MDecart - Decart)))
    if np.max(np.abs(MDecart - Decart)) < 1e-10:
        break
ssgm = sigma
print('Декартовы координаты \n',MDecart)
#print(np.diag(matrixD))
# print(sigma)

############### Доверительные области ###############

##### ММК(заполнение точек по всему объему эллипсода) #####

XE = []
YE = []
ZE = []
for i in range(kt):
    AHol = np.linalg.cholesky(matrixD)
    randNumb = np.matrix([random.normal(0, 1) for i in range(6)]).T
    DecartELP = MDecart + AHol * randNumb
    XE.append(DecartELP[0, 0])
    YE.append(DecartELP[1, 0])

##### формирование эллипсоида по граничной поверхности #####
AH = []
XX = []
YY = []
ZZ = []
AH2 = []

for i in range(kt):
    randNumb22 = np.matrix([random.normal(0, sigma**2) for i in range(6)]).T
    AHol2 = np.linalg.cholesky(matrixD)
    AHR2 = AHol * randNumb22
    for j in range(6):
        AHR2[j, 0] = (kk / sqrt(
            (randNumb22[0, 0] ** 2) + (randNumb22[1, 0] ** 2) + (randNumb22[2, 0] ** 2) + (randNumb22[3, 0] ** 2) + (
                        randNumb22[4, 0] ** 2) + (randNumb22[5, 0] ** 2))) * AHR2[j, 0]
    MDAHR2 = MDecart + AHR2
    AH2.append(MDAHR2)

for i in range(kt):
    randNumb2 = np.matrix([random.normal(0, 1) for i in range(6)]).T
    AHol = np.linalg.cholesky(matrixD)
    AHR = AHol * randNumb2
    for j in range(6):
        AHR[j, 0] = (kk / sqrt(
            (randNumb2[0, 0] ** 2) + (randNumb2[1, 0] ** 2) + (randNumb2[2, 0] ** 2) + (randNumb2[3, 0] ** 2) + (
                        randNumb2[4, 0] ** 2) + (randNumb2[5, 0] ** 2))) * AHR[j, 0]
    MDAHR = MDecart + AHR
    XX.append(MDAHR[0, 0])
    YY.append(MDAHR[1, 0])
    ZZ.append(MDAHR[2, 0])
    AH.append(MDAHR)

# ########## Вершины доверительного эллипсоида ##########
XM = []
YM = []
ZM = []
VerELP = []

DSobZ, DSobV = np.linalg.eig(matrixD)

DIAGV = np.diag(DSobZ)  # диагональная матрица с собственными значениями
MM = kk * sqrt(DIAGV) * DSobV.T

for i in range(6):
    Ver6 = MDecart + MM[i, :].T
    VerELP.append(Ver6)
    XM.append(Ver6[0, 0])
    YM.append(Ver6[1, 0])
    # ZM.append(Ver6[2,0])

for i in range(6):
    Ver12 = MDecart - MM[i, :].T
    VerELP.append(Ver12)
    XM.append(Ver12[0, 0])
    YM.append(Ver12[1, 0])
    # ZM.append(Ver12[2,0])
Ver1, Ver2, Ver3, Ver4, Ver5, Ver6, Ver7, Ver8, Ver9, Ver10, Ver11, Ver12 = VerELP
#print(VerELP)
# # plt.scatter(XE,YE,marker='o',color='yellow')
# # plt.scatter(XX,YY,marker='o',color='grey')
# # plt.scatter(XM,YM, marker='x', color='red')
# # plt.xlim((min(XX)-10**-7), (max(XX)+10**-7))
# # plt.ylim((min(YY)-10**-7), (max(YY)+10**-7))
# # fig = plt.figure()
# # ax = fig.add_subplot(111, projection='3d')
# # ax.scatter(XX, YY, ZZ)
# # ax.scatter(XE, YE, ZE)
# # ax.scatter(XM, YM, ZM)
# # plt.show()
#
# ########## Показатели нелинейности ##########
f0 = fq / (n2 * 2 - 6)
# sig = []
# fv = []
# for i in range(12):
#     Dec = Sat(VerELP[i].squeeze().tolist()[0], ptype='qv', mu=mu_solar_au, t0=t0)
#     orb = Dec.get('kep')
#     aster = Sat(orb, ptype='kep', mu=mu_solar_au, t0=t0)
#     alpha = []
#     delta = []
#     fq = 0
#     for j in range(n2):
#         AstRaz = aster.cartesian(tm[j])
#         ZemRaz = earth.cartesian(tm[j])
#         RazX = aster.x - earth.x
#         RazY = aster.y - earth.y
#         RazZ = aster.z - earth.z
#
#         RRaz = sqrt(RazX ** 2 + RazY ** 2 + RazZ ** 2)
#
#         alph = reduce(np.arctan2(RazY, RazX))
#         delt = arcsin(RazZ / RRaz)
#
#         fq = fq + ((alph - AlphaObs[j]) * cos(DeltaObs[j])) ** 2 + (delt - DeltaObs[j]) ** 2
#         fva = fq / (n2 * 2 - 6)
#         sigma = sqrt(fq / (2 * n2 - 6))
#     sig.append(sigma)
#     fv.append(fva)
#
# sg0 = 0
#
# for i in range(12):
#     sg0 = sg0 + sig[i]
#
# sg0 = sg0 / 12
#
# for i in range(12):
#     df = sg0 - sig
#
# sg0 = 0
#
# for i in range(12):
#     sg0 = sg0 + sig[i]
#
# sg0 = (sg0 - sig[0] - sig[6]) / 10
#
# avf0 = 0
# for i in range(12):
#     avf0 = avf0 + fv[i]
#
# avf0 = avf0 / 12
# for i in range(12):
#     df[i] = avf0 - fv[i]
#
# avf0 = 0
# for i in range(12):
#     avf0 = avf0 + fv[i]
#
# avf0 = (avf0 - fv[0] - fv[6]) / 10
#
# ff = f0 * (1 + (kk ** 2) / (2 * n2 - 6))
# kj = (sqrt(np.max(fv) - f0) / ssgm) * sqrt(2 * n2 - 6)
#
# x1 = (np.max(sig) - np.min(sig)) / (2 * (np.min(sig) - ssgm))
# x2 = (np.max(sig) - sg0) / (2 * (sg0 - ssgm))
# x3 = (np.max(fv) - np.min(fv)) / (2 * (np.min(fv) - f0))
# x4 = (np.max(fv) - avf0) / (2 * (avf0 - f0))
# x5 = ((np.max(fv)) - ff) / (2 * (ff - f0))
# x6 = (np.max(sig) - sqrt(ff)) / (2 * (sqrt(ff) - ssgm))
# x7 = ((sqrt(np.max(fv) - f0) * sqrt(2 * n2 - 6)) / (ssgm * kk)) - 1
# x8 = np.abs(kj - kk) / kk
# x9 = np.abs(kj - kk) / kj
# print('показатели нелинейновсти',x1,x2,x3,x4,x5,x6,x7,x8,x9)
#
# ########## Точные показатели нелинейности ##########
ff = f0 * (1 + (kk ** 2) / (2 * n2 - 6)) * (n2 * 2 - 6)
kaf = 0.1  # демпенговый множитель

DST = []
DLV = []
PNA = []
for i in tqdm.tqdm(range(12)):
    FactorL = 1
    DesDif = VerELP[i] - MDecart
    while True:
        fq = 0

        DesInt = MDecart + FactorL * DesDif
        DC = Sat(DesInt.squeeze().tolist()[0], ptype='qv', mu=mu_solar_au, t0=t0)
        orbt = DC.get('kep')
        aster = Sat(orbt, ptype='kep', mu=mu_solar_au, t0=t0)
        for j in range(n2):
            XZAI = aster.derivatives(tm[j])
            AstRaz = aster.cartesian(tm[j])
            ZemRaz = earth.cartesian(tm[j])

            RazX = aster.x - earth.x
            RazY = aster.y - earth.y
            RazZ = aster.z - earth.z

            RRaz = sqrt(RazX ** 2 + RazY ** 2 + RazZ ** 2)

            alph = reduce(np.arctan2(RazY, RazX))
            delt = arcsin(RazZ / RRaz)

            dalph_dx = -sin(alph) / (RRaz * cos(delt))
            ddelt_dx = -cos(alph) * sin(delt) / RRaz

            dalph_dy = cos(alph) / (RRaz * cos(delt))
            ddelt_dy = -sin(alph) * sin(delt) / RRaz

            dalph_dz = 0
            ddelt_dz = cos(delt) / RRaz

            # print(alph,AlphaObs)
            matrixL[2 * j - 1][0] = (alph - AlphaObs[j]) * cos(DeltaObs[j])
            matrixL[2 * j][0] = delt - DeltaObs[j]

            for i in range(6):
                matrixR[2 * j - 1][i] = (dalph_dx * XZAI[0, i] + dalph_dy * XZAI[1, i]) * cos(DeltaObs[j])
                matrixR[2 * j][i] = (ddelt_dx * XZAI[0, i] + ddelt_dy * XZAI[1, i] + ddelt_dz * XZAI[2, i])

            fq = fq + ((alph - AlphaObs[j]) * cos(DeltaObs[j])) ** 2 + (delt - DeltaObs[j]) ** 2

        RObrL = (matrixR * np.linalg.inv(aster.derivatives3(t0))).T * matrixL
        bf = RObrL.T * DesDif
        bf = bf[0, 0]
        factorLN = FactorL
        FactorL = factorLN - kaf * (fq - ff) / (2 * bf)
        #print(np.abs(FactorL - factorLN))
        if np.abs(FactorL - factorLN) < 10 ** -10:
            break
    PN = np.abs(VerELP[i] - (MDecart + FactorL * (VerELP[i] - MDecart))) / (
        np.abs((MDecart + FactorL * (VerELP[i] - MDecart)) - MDecart))
    #print(PN)
    DLV.append(FactorL)
    PNA.append(PN)
    DST.append(DesInt)
print('точные показатели нелинейности',PNA)


####### на уровенной #######
asterNew = Sat([a, e, i, W, w, M0], ptype='kep', mu=mu_solar_au, t0=t0)
rfe = np.matrix(asterNew.cartesian(t0+kd)).T
print(rfe -MDecart )
astTi = []
xast = []
yast = []
zast = []
for k in range(kt):
    DesInt = AH2[k]
    DoC = Sat(DesInt.squeeze().tolist()[0], ptype='qv', mu=mu_solar_au, t0=t0)
    ort = DoC.get('kep')
    aster = Sat(ort, ptype='kep', mu=mu_solar_au, t0=t0)
    med = np.matrix(aster.cartesian(t0+kd)).T
    astTi.append(med)
    xast.append(med[0, 0])
    yast.append(med[1, 0])
    zast.append(med[2, 0])
np.savetxt('XYZE.txt',np.column_stack([(xast-rfe[0,0])*(-1),(yast-rfe[1,0])*(-1),(zast-rfe[2,0])*(-1)]))

# xastd = []
# yastd = []
# zastd = []
#
# DIT = MDecart
# DoCoT = Sat(DIT.squeeze().tolist()[0], ptype='qv', mu=mu_solar_au, t0=t0)
# ortbt = DoCoT.get('kep')
# aster = Sat(ortbt, ptype='kep', mu=mu_solar_au, t0=t0)
# mead = np.matrix(aster.cartesian(t0+kd)).T
# xastd.append(mead[0, 0])
# yastd.append(mead[1, 0])
# zastd.append(mead[2, 0])

DL = []
DI = []

for k in tqdm.tqdm(range(kt)):
    FactorL = 1
    DesDif = AH2[k] - MDecart
    iterat = 0
    while True:
        fq = 0
        DesInt = MDecart + FactorL * DesDif
        # print(DesInt)
        DoC = Sat(DesInt.squeeze().tolist()[0], ptype='qv', mu=mu_solar_au, t0=t0)
        ort = DoC.get('kep')
        aster = Sat(ort, ptype='kep', mu=mu_solar_au, t0=t0)

        for j in range(n2):
            XZAI = aster.derivatives(tm[j])

            AstRaz = aster.cartesian(tm[j])
            ZemRaz = earth.cartesian(tm[j])

            RazX = aster.x - earth.x
            RazY = aster.y - earth.y
            RazZ = aster.z - earth.z

            RRaz = sqrt(RazX ** 2 + RazY ** 2 + RazZ ** 2)

            alph = reduce(np.arctan2(RazY, RazX))
            delt = arcsin(RazZ / RRaz)

            dalph_dx = -sin(alph) / (RRaz * cos(delt))
            ddelt_dx = -cos(alph) * sin(delt) / RRaz

            dalph_dy = cos(alph) / (RRaz * cos(delt))
            ddelt_dy = -sin(alph) * sin(delt) / RRaz

            dalph_dz = 0
            ddelt_dz = cos(delt) / RRaz

            # print(alph,AlphaObs)
            matrixL[2 * j - 1][0] = (alph - AlphaObs[j]) * cos(DeltaObs[j])
            matrixL[2 * j][0] = delt - DeltaObs[j]

            for i in range(6):
                matrixR[2 * j - 1][i] = (dalph_dx * XZAI[0, i] + dalph_dy * XZAI[1, i]) * cos(DeltaObs[j])
                matrixR[2 * j][i] = (ddelt_dx * XZAI[0, i] + ddelt_dy * XZAI[1, i] + ddelt_dz * XZAI[2, i])

            fq = fq + ((alph - AlphaObs[j]) * cos(DeltaObs[j])) ** 2 + (delt - DeltaObs[j]) ** 2

        RObrL = (matrixR * np.linalg.inv(aster.derivatives3(t0))).T * matrixL
        bf = RObrL.T * DesDif
        bf = bf[0, 0]
        factorLN = FactorL
        FactorL = factorLN - kaf * (fq - ff) / (2 * bf)
        iterat = iterat + 1
        # print(np.abs(FactorL - factorLN))
        if (np.abs(FactorL - factorLN) < 10 ** -6) or (iterat > 1000):
            break

    DL.append(FactorL)
    np.savetxt('L.txt', DL)
    DI.append(DesInt)


astgx = []
astgy = []
astgz = []
astgranTi = []
for k in range(kt):
    DesInt = DI[k]
    # print(DesInt)
    DoC = Sat(DesInt.squeeze().tolist()[0], ptype='qv', mu=mu_solar_au, t0=t0)
    ort = DoC.get('kep')
    aster = Sat(ort, ptype='kep', mu=mu_solar_au, t0=t0)
    med = np.matrix(aster.cartesian(t0+kd)).T
    astgranTi.append(med)
    astgx.append(med[0, 0])
    astgy.append(med[1, 0])
    astgz.append(med[2, 0])
np.savetxt('XYZG.txt',np.column_stack([(astgx-rfe[0,0])*(-1),(astgy-rfe[1,0])*(-1),(astgz-rfe[2,0])*(-1)]))

# astvx = []
# astvy = []
# astvz = []
#
# for k in range(12):
#     DesInt = DST[k]
#     DoC = Sat(DesInt.squeeze().tolist()[0], ptype='qv', mu=mu_solar_au, t0=t0)
#     ort = DoC.get('kep')
#     aster = Sat(ort, ptype='kep', mu=mu_solar_au, t0=t0)
#     med = np.matrix(aster.cartesian(t0+kd)).T
#
#     astvx.append(med[0, 0])
#     astvy.append(med[1, 0])
#     astvz.append(med[2, 0])

############### вероятность столкновения ##############
CH = []
t1 = 2456150.0

### ez ###
MDCT = MDecart
NMDCT = Sat(MDCT.squeeze().tolist()[0], ptype='qv', mu=mu_solar_au, t0=t0)
nobt = NMDCT.get('kep')
aster = Sat(nobt, ptype='kep', mu=mu_solar_au, t0=t0)
for i in range(90):
    tt = t1 + (i - 45) * 0.0001
    np.matrix(aster.cartesian(tt)).T
    np.matrix(earth.cartesian(tt)).T
    delvnom = sqrt((aster.vx - earth.vx)**2 + (aster.vy - earth.vy)**2 + (aster.vz - earth.vz)**2)
    vinf = 0.00645699
    radius = 0.000042781
    rzahnom = radius * sqrt(1+(vinf/delvnom)**2)
    delrnom = sqrt((aster.x - earth.x)**2 + (aster.y - earth.y)**2 + (aster.z - earth.z)**2)
    if (delrnom<rzahnom):
        print('Nominalnaya orbita stolknovitelna!')
    else:
        print('Nominalnaya orbita ne stolknovitelna!')

t1 = 2456150.0
verst = 0
for i in tqdm.tqdm(range(kt)):
    MDCT2 = AH2[i]
    NMDCT2 = Sat(MDCT2.squeeze().tolist()[0], ptype='qv', mu=mu_solar_au, t0=t0)
    nobt2 = NMDCT2.get('kep')
    aster = Sat(nobt2, ptype='kep', mu=mu_solar_au, t0=t0)
    for j in range(90):
        tt = t1 + (j - 45) * 0.0001
        aster.cartesian(tt)
        earth.cartesian(tt)
        delvnom = sqrt((aster.vx - earth.vx)**2 + (aster.vy - earth.vy)**2 + (aster.vz - earth.vz)**2)
        vinf = 0.00645699
        radius = 0.000042781
        rzahnom = radius * sqrt(1+(vinf/delvnom)**2)
        delrnom = sqrt((aster.x - earth.x)**2 + (aster.y - earth.y)**2 + (aster.z - earth.z)**2)
        if (delrnom<rzahnom):
            verst = verst + 1
            break
print(verst*100/kt,'%')

sgm1 = (1 / sqrt(matrixD[5,5]))


MDCT = MDecart
NMDCT = Sat(MDCT.squeeze().tolist()[0], ptype='qv', mu=mu_solar_au, t0=t0)

XHER = []
YHER = []

XVS = []
YVS = []
herak = Ver1
hera = []

for i in range(1000):
    herak = herak + (Ver7-Ver1)/1000
    hera.append(herak)
    XVS.append(herak[0,0])
    YVS.append(herak[1,0])

XHER2 = []
YHER2 = []
for i in range(1):
    NMDCT.M0 = NMDCT.M0 + sigma * 3
    nobt = NMDCT.get('kep')
    str = Sat(nobt, ptype='kep', mu=mu_solar_au, t0=t0)
    her = np.matrix(str.cartesian(t0+kd)).T
    XHER.append(her[0,0])
    YHER.append(her[1,0])
for i in range(1):
    NMDCT.M0 = NMDCT.M0 - sigma * 6
    nobt = NMDCT.get('kep')
    str = Sat(nobt, ptype='kep', mu=mu_solar_au, t0=t0)
    her = np.matrix(str.cartesian(t0+kd)).T
    XHER2.append(her[0,0])
    YHER2.append(her[1,0])

HER = []
NMDCT.M0 = NMDCT.M0 + 5 * sigma
for i in range(200):
    NMDCT.M0 = NMDCT.M0 - ( 5 * (sigma/100))
    nobt = NMDCT.get('kep')
    str = Sat(nobt, ptype='kep', mu=mu_solar_au, t0=t0)
    her = np.matrix(str.cartesian(t0+kd)).T
    HER.append(her)
    XHER.append(her[0,0])
    YHER.append(her[1,0])

verst = 0
for i in tqdm.tqdm(range(1000)):
    MDCT2 = hera[i]
    NMDCT2 = Sat(MDCT2.squeeze().tolist()[0], ptype='qv', mu=mu_solar_au, t0=t0)
    nobt2 = NMDCT2.get('kep')
    aster = Sat(nobt2, ptype='kep', mu=mu_solar_au, t0=t0)
    for j in range(90):
        tt = t1 + (j - 45) * 0.0001
        aster.cartesian(tt)
        earth.cartesian(tt)
        delvnom = sqrt((aster.vx - earth.vx)**2 + (aster.vy - earth.vy)**2 + (aster.vz - earth.vz)**2)
        vinf = 0.00645699
        radius = 0.000042781
        rzahnom = radius * sqrt(1+(vinf/delvnom)**2)
        delrnom = sqrt((aster.x - earth.x)**2 + (aster.y - earth.y)**2 + (aster.z - earth.z)**2)
        if (delrnom<rzahnom):
            verst = verst + 1
            break
print(verst*100/kt,'%')




# plt.scatter(xast,yast)
# # plt.scatter(XHER,YHER)
# # #plt.scatter(XHER2,YHER2)
# plt.scatter(XVS,YVS)
# plt.show()



# ########### графики ############
plt.scatter(xast, yast, color='blue')
plt.scatter(astgx, astgy, color='red')
# plt.scatter(astvx, astvy, color='yellow')
# plt.scatter(xastd, yastd, color='black')
plt.show()
#
# plt.scatter(xast, zast, color='blue')
# plt.scatter(astgx, astgz, color='red')
# plt.scatter(astvx, astvz, color='yellow')
# plt.scatter(xastd, zastd, color='black')
# plt.show()
#
# plt.scatter(yast, zast, color='blue')
# plt.scatter(astgy, astgz, color='red')
# plt.scatter(astvy, astvz, color='yellow')
# plt.scatter(yastd, zastd, color='black')
# plt.show()
#
#
ny = []
for i in range(kt):
    nuy = i + 1
    ny.append(nuy)

QW = np.ones(kt)
DLQW = DL - QW
plt.scatter(ny, DLQW , color = 'black')
plt.show()
nyv = []

for i in range(12):
    nv = i * round(kt/12)
    nyv.append(nv)

plt.scatter(ny,DL, color = 'black')
plt.scatter(nyv, DLV , color = 'red', marker='x')
plt.show()


