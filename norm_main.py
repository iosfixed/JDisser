import re

import numpy as np
from numpy import sqrt, pi, dot, sin, cos, arctan2, cross, arcsin, arctan, math, random

from utils import reduce, ephem
from consts import *

import matplotlib.pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

# Все параметры записывают перед процедурками
# t0 = 2451545.0

t0 = 2455647.46557825

a_real = 0.77687777440592381
e_real = 0.68510387692466500
i_real = 2.77916532987703325
W_real = 3.2670520191630
w_real = 0.326641461628737
M0_real = 4.80579307699276

a_0 = 0.7
e_0 = 0.6
i_0 = 2.7
W_0 = 3.2
w_0 = 0.3
M0_0 = 4.8

a_earth = 1.00006565717335842
e_earth = 0.01708278413879609
i_earth = 0.40910070729284629
W_earth = 0.00001440473697043
w_earth = -4.483954375681416
M0_earth = 1.409510836394793


class Sat:
	# 	t0 = 2451545.0

	def __init__(self, init=None, ptype='kep', t0=t0, mu=mu_geo):
		self.t0 = t0
		self.mu = mu

		if (ptype == 'kep'):
			if (init == None):
				init = [42164, 0, 0, 0, 0, 0]  # дефолт
			self.a, self.e, self.i, self.W, self.w, self.M0 = init
			self.cartesian()

	# Вычисления
	# Функция определения координат
	def cartesian(self, t=t0, dt=None):
		a, e, i, W, w, M0 = self.get('kep')
		mu = self.mu
		t0 = self.t0

		A = [[cos(W), -sin(W), 0],
		     [sin(W), cos(W), 0],
		     [0, 0, 1]]
		A = np.matrix(A)

		B = [[1, 0, 0],
		     [0, cos(i), -sin(i)],
		     [0, sin(i), cos(i)]]
		B = np.matrix(B)

		C = [[cos(w), -sin(w), 0],
		     [sin(w), cos(w), 0],
		     [0, 0, 1]]
		C = np.matrix(C)

		R = A * B * C  # поворотная матрица

		n = sqrt(mu * a ** (-3))

		if (dt):
			M = M0 + n * dt
		else:
			M = M0 + n * (t - t0)
		E = reduce(M)

		# Численное решение уравнения Кеплера

		while (abs(E - e * sin(E) - M) > 1e-12):
			E = E - (E - e * sin(E) - M) / (1 - e * cos(E))

		q = np.matrix([a * (cos(E) - e), a * sqrt(1 - e ** 2) * sin(E), 0])
		dq = np.matrix([-a * n * sin(E) / (1 - e * cos(E)), a * sqrt(1 - e ** 2) * cos(E) * n / (1 - e * cos(E)), 0])

		#        reduced = reduce(M) - M0

		q = R * q.T
		dq = R * dq.T

		self.x, self.y, self.z = q.A1
		self.vx, self.vy, self.vz = dq.A1
		return [self.x, self.y, self.z, self.vx, self.vy, self.vz]

	# Функция для вичисления частных производных по кеплеровым элементам
	def findOrbit(self):
		q = [self.x, self.y, self.z]
		v = [self.vx, self.vy, self.vz]

		r = sqrt(dot(q, q))
		V = sqrt(dot(v, v))
		s = dot(q, v)
		h = cross(q, v)

		k = sqrt(self.mu)

		# Большая полуось
		a = 1 / abs(2. / r - V ** 2 / k ** 2)

		# Эксцентрисистет
		e = sqrt(s * s / (k * k * a) + (1 - r / a) ** 2)

		# Средняя аномалия:
		dy = s / (e * k * sqrt(a))
		dx = (a - r) / (a * e)
		E0 = arctan2(dy, dx)
		M0 = E0 - e * sin(E0)

		# Долгота восходящего узла:
		W = arctan2(h[0], -h[1])

		# Наклонение
		i = arctan2(sqrt(h[0] ** 2 + h[1] ** 2), h[2])

		# Аргумент перицентра:
		p = a * (1 - e ** 2)

		dy = sqrt(p) * s
		dx = k * (p - r)
		vv = arctan2(dy, dx)

		if (sin(i) != 0):
			dy = self.z / sin(i)
			dx = self.x * cos(W) + self.y * sin(W)
			uu = arctan2(dy, dx)
		else:
			uu = 0

		w = uu - vv
		while (w < 0):
			w += 2 * pi

		self.a, self.e, self.i, self.W, self.w, self.M0 = a, e, i, W, w, M0
		return [self.a, self.e, self.i, self.W, self.w, self.M0]

	def alpha_delta(self, t):
		x, y, z, vx, vy, vz = aster.cartesian(t)
		xr = aster.x - earth.x
		yr = aster.y - earth.y
		zr = aster.z - earth.z

		r = sqrt(xr ** 2 + yr ** 2 + zr ** 2)

		alpha = reduce(np.arctan2(yr, xr))
		delta = arcsin(zr / r)
		return alpha, delta

	def derivatives(self, t=t0, dt=None):
		a, e, i, W, w, M0 = self.get('kep')
		mu = self.mu
		t0 = self.t0

		A = [[cos(W), -sin(W), 0],
		     [sin(W), cos(W), 0],
		     [0, 0, 1]]
		A = np.matrix(A)

		B = [[1, 0, 0],
		     [0, cos(i), -sin(i)],
		     [0, sin(i), cos(i)]]
		B = np.matrix(B)

		C = [[cos(w), -sin(w), 0],
		     [sin(w), cos(w), 0],
		     [0, 0, 1]]
		C = np.matrix(C)

		R = A * B * C

		n = sqrt(mu * a ** (-3))

		if (dt):
			M = M0 + n * dt
		else:
			M = M0 + n * (t - t0)

		E = reduce(M)

		# Численное решение уравнения Кеплера

		while (abs(E - e * sin(E) - M) > 1e-12):
			E = E - (E - e * sin(E) - M) / (1 - e * cos(E))

		reduced = n * (t - t0)  # M - M0

		q = np.matrix([a * (cos(E) - e), a * sqrt(1 - e ** 2) * sin(E), 0])
		dksi_da = cos(E) - e + 3 / 2 * (reduced * sin(E)) / (1 - e * cos(E))
		deta_da = sqrt(1 - e ** 2) * (sin(E) - 3 / 2 * (reduced * cos(E)) / (1 - e * cos(E)))
		da = np.matrix([dksi_da, deta_da, 0])
		dxyz_da = R * da.T

		dksi_de = -a * (1 + (sin(E) ** 2) / (1 - e * cos(E)))
		deta_de = a * sqrt(1 - e ** 2) * sin(E) * ((cos(E) / (1 - e * cos(E))) - (e / (1 - e ** 2)))
		de = np.matrix([dksi_de, deta_de, 0])
		dxyz_de = R * de.T

		dksi_dM0 = - (a * sin(E)) / (1 - e * cos(E))
		deta_dM0 = (a * (sqrt(1 - e ** 2)) * cos(E)) / (1 - e * cos(E))
		dM0 = np.matrix([dksi_dM0, deta_dM0, 0])
		dxyz_M0 = R * dM0.T

		dB = [[0, 0, 0],
		      [0, -sin(i), -cos(i)],
		      [0, cos(i), -sin(i)]]
		dB = np.matrix(dB)

		dA = [[-sin(W), -cos(W), 0],
		      [cos(W), -sin(W), 0],
		      [0, 0, 0]]
		dA = np.matrix(dA)

		dC = [[-sin(w), -cos(w), 0],
		      [cos(w), -sin(w), 0],
		      [0, 0, 0]]
		dC = np.matrix(dC)

		dxyz_di = A * dB * C * q.T
		dxyz_dw = A * B * dC * q.T
		dxyz_dW = dA * B * C * q.T

		dxyz_da_dM0 = np.concatenate([dxyz_da, dxyz_de, dxyz_di, dxyz_dw, dxyz_dW, dxyz_M0], axis=1)

		return np.asmatrix(dxyz_da_dM0)

	# Функция определения частных производных по alpha, delta
	def derivatives2(self, t=t0, separate_angles=False):
		self.cartesian(t)
		xr = aster.x - earth.x
		yr = aster.y - earth.y
		zr = aster.z - earth.z

		r = sqrt(xr ** 2 + yr ** 2 + zr ** 2)

		alpha = reduce(np.arctan2(yr, xr))
		delta = arcsin(zr / r)

		dalpha_dx = -sin(alpha) / (r * cos(delta))
		ddelta_dx = -cos(alpha) * sin(delta) / r

		dalpha_dy = cos(alpha) / (r * cos(delta))
		ddelta_dy = -sin(alpha) * sin(delta) / r

		dalpha_dz = 0
		ddelta_dz = cos(delta) / r

		dalpha_dq = np.matrix([dalpha_dx, dalpha_dy, dalpha_dz])
		ddelta_dq = np.matrix([ddelta_dx, ddelta_dy, ddelta_dz])

		if not separate_angles:
			dalpha_dq_ddelta_dq = np.matrix(dalpha_dq * cos(delta) + ddelta_dq)
		else:
			dalpha_dq_ddelta_dq = np.concatenate([dalpha_dq, ddelta_dq], axis=0)

		return np.asmatrix(dalpha_dq_ddelta_dq)

	def derivatives3(self, t=t0, dt=None):
		a, e, i, W, w, M0 = self.get('kep')
		mu = self.mu
		t0 = self.t0

		A = [[cos(W), -sin(W), 0],
		     [sin(W), cos(W), 0],
		     [0, 0, 1]]
		A = np.matrix(A)

		B = [[1, 0, 0],
		     [0, cos(i), -sin(i)],
		     [0, sin(i), cos(i)]]
		B = np.matrix(B)

		C = [[cos(w), -sin(w), 0],
		     [sin(w), cos(w), 0],
		     [0, 0, 1]]
		C = np.matrix(C)

		R = A * B * C

		n = sqrt(mu * a ** (-3))

		if (dt):
			M = M0 + n * dt
		else:
			M = M0 + n * (t - t0)

		E = reduce(M)

		# Численное решение уравнения Кеплера

		while (abs(E - e * sin(E) - M) > 1e-12):
			E = E - (E - e * sin(E) - M) / (1 - e * cos(E))

		q = np.matrix([a * (cos(E) - e), a * sqrt(1 - e ** 2) * sin(E), 0])
		dq = np.matrix([-a * n * sin(E) / (1 - e * cos(E)), a * sqrt(1 - e ** 2) * cos(E) * n / (1 - e * cos(E)), 0])
		reduced = n * (t - t0)  # M - M0

		# частные производные
		dksi_da = cos(E) - e + 3 / 2 * (reduced * sin(E)) / (1 - e * cos(E))
		ddksi_da = 1 / 2 * n * (sin(E) / (1 - e * cos(E))) + 3 / 2 * n * reduced * (
		(cos(E) - e) / (1 - e * cos(E)) ** 3)
		deta_da = sqrt(1 - e ** 2) * (sin(E) - 3 / 2 * (reduced * cos(E)) / (1 - e * cos(E)))
		ddeta_da = - (n * sqrt(1 - e * e) * cos(E)) / (2 * (1 - e * cos(E))) + 3 / 2 * n * sqrt(
			1 - e ** 2) * reduced * (sin(E) / (1 - e * cos(E)) ** 3)
		da = np.matrix([dksi_da, deta_da, 0])
		dda = np.matrix([ddksi_da, ddeta_da, 0])
		dxyz_da = R * da.T
		dxyz_dda = R * dda.T

		dksi_de = -a * (1 + (sin(E) ** 2) / (1 - e * cos(E)))
		ddksi_de = - ((a * n * sin(E)) / (1 - e * cos(E)) ** 2) * (cos(E) + (cos(E) - e) / (1 - e * cos(E)))
		deta_de = a * sqrt(1 - e ** 2) * sin(E) * ((cos(E) / (1 - e * cos(E))) - (e / (1 - e ** 2)))
		ddeta_de = - (a * e * n * cos(E)) / (sqrt(1 - e ** 2) * (1 - e * cos(E))) - ((a * n * sqrt(1 - e ** 2)) / (
		1 - e * cos(E)) ** 2) * (sin(E) * sin(E) - cos(E) * (cos(E) - e) / (1 - e * cos(E)))
		de = np.matrix([dksi_de, deta_de, 0])
		dde = np.matrix([ddksi_de, ddeta_de, 0])
		dxyz_de = R * de.T
		dxyz_dde = R * dde.T

		dksi_dM0 = - (a * sin(E)) / (1 - e * cos(E))
		ddksi_dM0 = - (n * a * (cos(E) - e)) / (1 - e * cos(E)) ** 3
		deta_dM0 = (a * (sqrt(1 - e ** 2)) * cos(E)) / (1 - e * cos(E))
		ddeta_dM0 = - (n * a * sqrt(1 - e ** 2) * sin(E)) / (1 - e * cos(E)) ** 3
		dM0 = np.matrix([dksi_dM0, deta_dM0, 0])
		dxyz_M0 = R * dM0.T
		ddM0 = np.matrix([ddksi_dM0, ddeta_dM0, 0])
		ddxyz_M0 = R * ddM0.T

		#		print('dxyz/dM0 \n',dM0.T)

		dB = [[0, 0, 0],
		      [0, -sin(i), -cos(i)],
		      [0, cos(i), -sin(i)]]
		dB = np.matrix(dB)

		dA = [[-sin(W), -cos(W), 0],
		      [cos(W), -sin(W), 0],
		      [0, 0, 0]]
		dA = np.matrix(dA)

		dC = [[-sin(w), -cos(w), 0],
		      [cos(w), -sin(w), 0],
		      [0, 0, 0]]
		dC = np.matrix(dC)

		dxyz_di = A * dB * C * q.T
		dxyz_dw = A * B * dC * q.T
		dxyz_dW = dA * B * C * q.T
		dxyz_ddi = A * dB * C * dq.T
		dxyz_ddw = A * B * dC * dq.T
		dxyz_ddW = dA * B * C * dq.T

		dxyz_da_dM0 = np.concatenate([dxyz_da, dxyz_de, dxyz_di, dxyz_dw, dxyz_dW, dxyz_M0], axis=1)
		dxyz_dda_ddM0 = np.concatenate([dxyz_dda, dxyz_dde, dxyz_ddi, dxyz_ddw, dxyz_ddW, ddxyz_M0], axis=1)
		dxyz_da_dM0_dda_ddM0 = np.concatenate([dxyz_da_dM0, dxyz_dda_ddM0])
		return dxyz_da_dM0_dda_ddM0

	def RtDL(self, t, alpha_obs, delta_obs):
		alpha, delta = self.alpha_delta(t)
		dalpha = alpha - alpha_obs
		ddelta = delta - delta_obs

		R = self.derivatives2(t, separate_angles=True) * self.derivatives(t)
		R = np.asarray(R)
		row = R[0]*dalpha*cos(delta_obs)**2 + R[1]*ddelta
		return row[:, np.newaxis]

	# Процедуры установки новых параметров
	def set(self, settings, type='kep'):
		if (type == 'kep'):
			self.a, self.e, self.i, self.W, self.w, self.M0 = settings
			self.cartesian()

		if (type == 'qv'):
			self.x, self.y, self.z, self.vx, self.vy, self.vz = settings
			self.findOrbit()

	# Процедуры возвращающие параметры:
	def get(self, type='kep'):
		if (type == 'kep'):
			return [self.a, self.e, self.i, self.W, self.w, self.M0]


aster = Sat([a_real, e_real, i_real, W_real, w_real, M0_real], ptype='kep', mu=mu_solar_au, t0=t0)
earth = Sat([a_earth, e_earth, i_earth, W_earth, w_earth, M0_earth], ptype='kep', mu=mu_solar_au, t0=t0)

time = []
interval = 5
n2 = 55

Decart = np.matrix(aster.cartesian(t0)).T

for j in range(n2):
	arr = (t0 + ((j + 1) - math.trunc(n2 / 2)) * interval)
	time.append(arr)

ErrAlpha = []
ErrAlpha = (np.loadtxt('modalpha.txt')) * rad / 3600
# print(ErrAlpha)

ErrDelta = []
ErrDelta = (np.loadtxt('moddelta.txt')) * rad / 3600
# print(ErrDelta)

alpha = []
delta = []

for t in time:
	AstRaz = aster.cartesian(t)
	ZemRaz = earth.cartesian(t)

	RazX = aster.x - earth.x
	RazY = aster.y - earth.y
	RazZ = aster.z - earth.z

	RRaz = sqrt(RazX ** 2 + RazY ** 2 + RazZ ** 2)

	alph = reduce(np.arctan2(RazY, RazX))
	alpha.append(alph)

	delt = arcsin(RazZ / RRaz)
	delta.append(delt)

alpha = np.array(alpha)[:, np.newaxis]
delta = np.array(delta)[:, np.newaxis]

np.random.seed(666)
alpha_err = np.random.normal(0, 0.1) * rad / 3600
delta_err = np.random.normal(0, 0.1) * rad / 3600

AlphaObs = alpha + alpha_err
DeltaObs = delta + delta_err

XZAI = aster.derivatives(t0)
XZDA = aster.derivatives2(t0)

fq = 0
aster = Sat([a_0, e_0, i_0, W_0, w_0, M0_0], ptype='kep', mu=mu_solar_au, t0=t0)

R = np.ndarray(shape=(0, 6))
RTdL = np.zeros(shape=(6, 1))

for obs, t in enumerate(time):
	aster.cartesian(t)
	R = np.append(R, aster.derivatives2(t) * aster.derivatives(t), axis=0)
	RTdL += aster.RtDL(t, AlphaObs[obs], DeltaObs[obs])

dq = np.linalg.inv(R.T*R)*RtDL
print("")

# # Нормальная матрица
# for k in range(6):
#     for p in range(6):
#         RTR[k][p] = RTR[k][p] + (dalph_dx*XZAI[0,k] +dalph_dy*XZAI[1,k])*(dalph_dx*XZAI[0,p]+dalph_dy*XZAI[1,p])*(cos(DeltaObs[j]))*(cos(DeltaObs[j])) +(ddelt_dx*XZAI[0,k]+ddelt_dy*XZAI[1,k]+ddelt_dz*XZAI[2,k])*(ddelt_dx*XZAI[0,p]+ddelt_dy*XZAI[1,p]+ddelt_dz*XZAI[2,p])
#
#     for i_real in range(6):
#         RTL[i_real] = RTL[i_real] + (dalph_dx * XZAI[0, i_real] + dalph_dy * XZAI[1, i_real]) * (alph - AlphaObs[j]) * (cos(DeltaObs[j])) * (cos(DeltaObs[j])) + (ddelt_dx * XZAI[0, i_real] + ddelt_dy * XZAI[1, i_real] + ddelt_dz * XZAI[2, i_real]) * (delt - DeltaObs[j])
#
#
#     matrixL[2*j - 1][0] = (alph - AlphaObs[j])* cos(DeltaObs[j])
#     matrixL[2*j][0] = delt - DeltaObs[j]
#
#     for i_real in range(6):
#         matrixR[2*j-1][i_real] = (dalph_dx * XZAI[0, i_real] + dalph_dy * XZAI[1, i_real]) * cos(DeltaObs[j])
#         matrixR[2*j][i_real] = (ddelt_dx * XZAI[0, i_real] + ddelt_dy * XZAI[1, i_real] + ddelt_dz * XZAI[2, i_real])
#
#     fq = fq + ((alph - AlphaObs[j])* cos(DeltaObs[j]))**2 + (delt-DeltaObs[j])**2
#
#
# ObrRTR = np.linalg.inv(RTR)
# #print(ObrRTR)
#
# #Тут что-то происходит непонятное
#
# RORT =(aster.derivatives3(t0)) * ObrRTR * (aster.derivatives3(t0)).T
# ORORT = np.linalg.inv(RORT)
# RObrL = (matrixR * np.linalg.inv(aster.derivatives3(t0))).T * matrixL
# Mod = RORT * RObrL
# MDecart = Decart - Mod
#
#
# while True:
#     if np.max(np.abs(MDecart - Decart) < 10**-12):
#         Decart = MDecart
#     break
